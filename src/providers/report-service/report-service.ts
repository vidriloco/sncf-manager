import { Injectable } from '@angular/core';

/*
  Generated class for the ReportServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ReportServiceProvider {
	
	constructor() {}
	
	humanizedType(enumedType: string) {
		switch(enumedType) { 
		   case 'broken': { 
				 return "Broken/soiled train";
		   } 
		   case 'bad-smell': { 
				 return "Smell";
		   }
		   case 'temperature': { 
				 return "Temperature"
		   } 
		   case 'noisy': { 
				 return "Noise";
		   } 
		   default: { 
		     return "Other";
		   } 
		} 
	}
	
	iconFor(enumedType: string) {
		switch(enumedType) { 
		   case 'broken': { 
				 return "fix-icon-square.jpg";
		   } 
		   case 'bad-smell': { 
				 return "smell-icon-square.jpg";
		   }
		   case 'temperature': { 
				 return "temp-square.jpg"
		   } 
		   case 'noisy': { 
				 return "noise-square.jpg";
		   } 
		   case 'other': { 
				 return "3-dots-square.png";
		   } 
		   default: { 
		     return "3-dots-square.png";
		   } 
		}
	}
	
	reporterPic(personSlug: string) {
		switch(personSlug) { 
		   case 'lady-gagus': { 
				 return "lady-gagus.jpeg";
		   } 
		   case 'tim-cooking': { 
				 return "tim-cooking.jpeg";
		   }
		   case 'thomas-rutte': { 
				 return "rutte.jpeg";
		   }
			 default: {
				 return "rutte.jpeg";
			 }
		}
	}
	

}
