import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetailsPage } from '../details/details';
import { Http } from '@angular/http';
import { ReportServiceProvider } from '../../providers/report-service/report-service';

import * as Constants from '../../config/constants';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
	openIssues:  any;
	solvedIssues: any;
	
  constructor(public navCtrl: NavController, public http: Http, public reportService: ReportServiceProvider) {
		var object = this;
		this.http.get(Constants.urlFor('reports/open.json')).subscribe(res => { 
			object.openIssues = res.json();
		});
		
		this.http.get(Constants.urlFor('reports/closed.json')).subscribe(res => { 
			object.solvedIssues = res.json();
		});
  }
	
  displayIssueDetails(id: number) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(DetailsPage, { reportId: id });
  }
	
	closedIssuesList() {
		console.log(this.solvedIssues);
		if(this.solvedIssues == null) {
			return [];
		}
		return this.solvedIssues;
	}
	
  openIssuesList() {
		if(this.openIssues == null) {
			return 0;
		}
		return this.openIssues;
  }
}
