import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { ReportServiceProvider } from '../../providers/report-service/report-service';

import * as Constants from '../../config/constants';

@Component({
  selector: 'page-details',
  templateUrl: 'details.html'
})
export class DetailsPage {
	currentReport: any;
	
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public reportService: ReportServiceProvider) {
		var object = this;
    let reportId = this.navParams.get('reportId');

		this.http.get(Constants.urlFor('reports/'+ reportId +'.json')).subscribe(res => { 
			object.currentReport = res.json();
		});
		
  }
}
