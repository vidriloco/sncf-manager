import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';

import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import * as Constants from '../config/constants';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;
	openIssuesCount = 0;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public http: Http) {
    this.initializeApp();
		
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Manage', component: HomePage }
    ];
		this.http.get(Constants.urlFor('reports/open.json')).subscribe(res => { 
			this.openIssuesCount = res.json().length;
		});
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  outstandingIssues() {
    return this.openIssuesCount;
  }
}
